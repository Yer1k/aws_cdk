# Welcome to my CDK TypeScript project

## CI/CD Status of both Build and Deploy to AWS
[![CI/CD status](https://gitlab.com/Yer1k/aws_cdk/badges/main/pipeline.svg)](https://gitlab.com/Yer1k/aws_cdk/-/commits/main)


This is a simple project for CDK development with TypeScript with AWS CDK and S3 bucket.

## Features
- Use AWS Codewhisperer to create a new CDK project
- Create a S3 bucket with CDK 
- Deploy to AWS using gitlab CI/CD with AWS CLI and masked variables on GitLab

## Flowchart
![Flowchart](./screenshots/flowchart.png)

## Screenshots
In this project, as for education purpose, I used the following CodeWhisperer prompts to generate the code in typescript.

First, I asked CodeWhisperer what APPS we can create with AWS CDK with Amazon Q:
![CodeWhisperer Prompt 1](./screenshots/cw_p1.png)

Then, since we want to create S3 bucket using AWS CDK, by just typing "create a s3 bucket with aws cdk" to codewhisperer, it generated the following code:
![CodeWhisperer Pormpt 2](./screenshots/cw_p2.png)

OR, on the code file, I can just comment and use code whisperer to generate the code:
![CodeWhisperer Pormpt 3](./screenshots/cw_p3.png)

S3 Bucket created using AWS CDK:
![S3 Bucket](./screenshots/s3_bucket.png)
![S3 Bucket Object](./screenshots/s3_bucket2.png)

Using Gitlab CI/CD to deploy the code to AWS::
![Gitlab CI/CD](./screenshots/gitlab_cicd1.png)

