import * as cdk from 'aws-cdk-lib';
import { aws_s3 as s3 } from 'aws-cdk-lib';

export class AwsCdkStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    // create a new s3 bucket
    new s3.Bucket(this, 'cdkBucket', {
      versioned: true,
      // add encryption to the bucket
      encryption: s3.BucketEncryption.S3_MANAGED,
    });
  }
}
